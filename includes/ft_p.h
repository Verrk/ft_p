/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 22:28:20 by cpestour          #+#    #+#             */
/*   Updated: 2015/06/11 10:08:48 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_P_H
# define FT_P_H

# include <stdio.h>
# include <sys/types.h>
# include <sys/param.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <sys/socket.h>
# include <netdb.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <libft.h>
# include <time.h>
# include <signal.h>

# define EV(err, res, str)	(e_void(err, res, str))
# define E(err, res, str)	(e_int(err, res, str))
# define FLAGS			O_CREAT | O_TRUNC | O_WRONLY
# define MODE			S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH

# define GREEN			"\e[32m"
# define RED    		"\e[31m"
# define YELLOW			"\e[33m"
# define BLUE			"\e[34m"
# define RESET			"\033[0m"

typedef struct			s_env
{
	int					port;
	int					s;
	int					cs;
	char				buf[1024];
	char				*rootdir;
	char				*currdir;
	int					r;
	int					nc;
}						t_env;

void					*e_void(void *err, void *res, char *str);
int						e_int(int err, int res, char *str);
void					get_opt(t_env *e, int ac, char **av);
void					srv_create(t_env *e);
void					main_loop(t_env *e);
int						ft_read(int fd, char *buf);
void					ft_prompt(int status);
void					ft_cd(t_env *e);
void					ft_ls(t_env *e);
void					ft_put(t_env *e);
void					ft_get(t_env *e);
int						handle_file(int sock, char **args);

#endif
