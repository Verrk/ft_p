/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strprefix.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 08:00:17 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/27 08:07:17 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int			ft_strprefix(char *prefix, char *str)
{
	while (*prefix && *str && *str == *prefix)
	{
		prefix++;
		str++;
	}
	return ((*prefix == '\0'));
}
