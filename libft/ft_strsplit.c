/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/26 12:53:06 by verrk             #+#    #+#             */
/*   Updated: 2014/10/15 22:31:35 by verrk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_trim(char const *s, char c)
{
	char	*new;
	char	*end;

	new = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1));
	end = (char *)s + ft_strlen(s) - 1;
	while (*s == c)
		s++;
	if (*s == '\0')
	{
		free(new);
		return (NULL);
	}
	while (*end == c)
		end--;
	*(end + 1) = '\0';
	new = ft_strcpy(new, s);
	return (new);
}

static int	nb_word(char const *s, char c)
{
	int		i;
	int		ret;

	i = 0;
	ret = 1;
	if (*s == 0)
		return (0);
	while (s[i])
	{
		if (s[i] == c)
		{
			while (s[i] == c)
				i++;
			ret++;
		}
		else
			i++;
	}
	return (ret);
}

static int	ft_strlen_sep(char *s, char c)
{
	int		i;

	i = 0;
	while (s[i] != c && s[i])
		i++;
	return (i);
}

static char	*get_word(char **s, char c)
{
	char	*word;
	int		i;

	word = (char *)malloc(sizeof(char) * (ft_strlen_sep(*s, c) + 1));
	i = 0;
	while (**s != c && **s)
	{
		word[i] = **s;
		i++;
		(*s)++;
	}
	word[i] = '\0';
	while (**s == c)
		(*s)++;
	return (word);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**tab;
	char	*s_trim;
	int		i;
	int		nb;
	int		len;

	s_trim = ft_trim(s, c);
	if (!s_trim)
		return (NULL);
	nb = nb_word(s_trim, c);
	len = ft_strlen(s_trim);
	i = 0;
	tab = (char **)malloc(sizeof(char *) * (nb + 1));
	while (i < nb)
	{
		tab[i] = get_word(&s_trim, c);
		i++;
	}
	tab[i] = NULL;
	free(s_trim - len);
	return (tab);
}
