/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 20:34:16 by cpestour          #+#    #+#             */
/*   Updated: 2014/10/07 08:32:28 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrchr(const char *s, int c)
{
	char	*ret;

	ret = 0;
	if (*s == (char)c)
		ret = (char *)s;
	while (*s++)
	{
		if (*s == (char)c)
			ret = (char *)s;
	}
	return (ret);
}
