#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/13 22:39:14 by cpestour          #+#    #+#              #
#    Updated: 2015/09/09 19:02:22 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC=gcc
CFLAGS=-Wall -Werror -Wextra -g -Iincludes -Ilibft/includes
LDFLAGS=-Llibft -lft
S_DIR=srcs/serveur/
C_DIR=srcs/client/
S_FILES=main.c main_loop.c srv_create.c get_opt.c cmd.c
C_FILES=main.c file.c prompt.c
S_SRC=$(addprefix $(S_DIR), $(S_FILES))
C_SRC=$(addprefix $(C_DIR), $(C_FILES))
S_OBJ=$(S_SRC:.c=.o)
C_OBJ=$(C_SRC:.c=.o)

all: lib serveur client

lib:
	make -C libft

serveur: $(S_OBJ) srcs/utils.o
	$(CC) -o $@ $^ $(LDFLAGS)

client: $(C_OBJ) srcs/utils.o
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

srcs/utils.o: srcs/utils.c
	$(CC) -o $@ -c $^ $(CFLAGS)

clean:
	make clean -C libft
	rm -f srcs/*~ srcs/utils.o srcs/serveur/*~ srcs/client/*~ includes/*~ *~
	rm -f $(S_OBJ) $(C_OBJ)

fclean: clean
	make fclean -C libft
	rm -f serveur client

re: fclean all
