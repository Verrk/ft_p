/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/07 19:18:06 by cpestour          #+#    #+#             */
/*   Updated: 2015/06/14 09:49:15 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

static void			ft_puttime(char *t)
{
	int				i;

	i = 11;
	ft_putstr(YELLOW);
	while (i < 19)
		ft_putchar(t[i++]);
	ft_putstr(RESET);
}

void				ft_prompt(int status)
{
	time_t			t;

	time(&t);
	ft_puttime(ctime(&t));
	if (status == 0)
		ft_putstr(GREEN);
	else
		ft_putstr(RED);
	ft_putstr("$ ");
	ft_putstr(RESET);
}
