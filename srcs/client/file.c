/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 15:26:45 by cpestour          #+#    #+#             */
/*   Updated: 2015/09/09 19:27:02 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

static int		get_file(int sock, char **args)
{
	char		buf[1024];
	int			fd;
	int			r;
	char		*file;

	ft_read(sock, buf);
	if (ft_strcmp("KO", buf) == 0)
	{
		ft_putendl("ERROR: get");
		return (0);
	}
	file = ft_strrchr(args[1], '/');
	file = !file ? args[1] : file + 1;
	fd = open(file, FLAGS, MODE);
	while ((r = read(sock, buf, 1023)) > 0)
	{
		buf[r] = 0;
		ft_putstr_fd(buf, fd);
		if (r < 1023)
			break ;
	}
	close(fd);
	write(sock, "OK\0", 3);
	return (1);
}

static int		put_file(int sock, char **args)
{
	int			fd;
	int			r;
	char		buf[1024];

	if ((fd = open(args[1], O_RDONLY)) < 0)
	{
		write(sock, "KO\0", 3);
		ft_putendl("ERROR: put");
		return (0);
	}
	write(sock, "OK\0", 3);
	while ((r = read(fd, buf, 1023)) > 0)
	{
		buf[r] = 0;
		write(sock, buf, r);
	}
	close(fd);
	return (1);
}

int				handle_file(int sock, char **args)
{
	int			res;

	if (!args[1])
	{
		ft_putendl("ERROR: get: missing arg");
		return (0);
	}
	if (ft_strcmp(args[0], "put") == 0)
		res = put_file(sock, args);
	else
		res = get_file(sock, args);
	return (res);
}
