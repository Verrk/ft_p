/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 22:29:27 by cpestour          #+#    #+#             */
/*   Updated: 2015/06/14 09:51:53 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

static void				usage(char *str)
{
	printf("Usage: %s <adresse> <port>\n", str);
	exit(1);
}

static int				create_client(char *addr, int port)
{
	int					sock;
	struct protoent		*pe;
	struct sockaddr_in	sin;

	if (ft_strcmp("localhost", addr) == 0)
		addr = ft_strdup("127.0.0.1");
	pe = (struct protoent *)EV(NULL, getprotobyname("tcp"), "getprotobyname");
	sock = E(-1, socket(PF_INET, SOCK_STREAM, pe->p_proto), "socket client");
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = inet_addr(addr);
	E(-1, connect(sock, (struct sockaddr *)&sin, sizeof(sin)), "connect");
	return (sock);
}

static void				get_result(int sock, int *status)
{
	char				buf[1024];
	int					r;

	r = ft_read(sock, buf);
	ft_putstr(buf);
	if (r == 1024)
		get_result(sock, status);
	if (ft_strstr(buf, "SUCCESS"))
		*status = 0;
	else
		*status = 1;
}

static int				send_cmd(int sock, char *line, int *status)
{
	char				**args;
	int					res;

	res = 1;
	args = ft_strsplit_space(line);
	if (!args[0])
	{
		ft_free_tab(args);
		return (0);
	}
	if (ft_strcmp(line, "quit") == 0)
	{
		ft_free_tab(args);
		return (2);
	}
	write(sock, line, ft_strlen(line) + 1);
	if (ft_strcmp(args[0], "put") == 0 || ft_strcmp(args[0], "get") == 0)
		res = handle_file(sock, args);
	if (res)
		get_result(sock, status);
	else
		*status = 1;
	ft_free_tab(args);
	return (1);
}

int						main(int ac, char **av)
{
	int					port;
	int					sock;
	char				*line;
	int					res;
	int					status;

	if (ac != 3)
		usage(av[0]);
	status = 0;
 	port = ft_atoi(av[2]);
	sock = create_client(av[1], port);
	ft_prompt(status);
	while (get_next_line(0, &line))
	{
		if ((res = send_cmd(sock, line, &status)) == 2)
		{
			free(line);
			ft_putendl("SUCCESS: quit");
			break ;
		}
		free(line);
		ft_prompt(status);
	}
	close(sock);
	return (0);
}
