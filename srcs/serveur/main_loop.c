/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/18 05:40:53 by cpestour          #+#    #+#             */
/*   Updated: 2015/12/18 18:17:26 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

static void	get_cmd(t_env *e)
{
	char	**args;

	while (ft_read(e->cs, e->buf) > 0)
	{
		args = ft_strsplit_space(e->buf);
		if (ft_strcmp(args[0], "ls") == 0)
			ft_ls(e);
		else if (ft_strcmp(args[0], "pwd") == 0 && !args[1])
		{
			ft_putendl_fd(getcwd(NULL, 0), e->cs);
			write(e->cs, "SUCCESS: pwd\n\0", 14);
		}
		else if (ft_strcmp(args[0], "cd") == 0)
			ft_cd(e);
		else if (ft_strcmp(args[0], "put") == 0)
			ft_put(e);
		else if (ft_strcmp(args[0], "get") == 0)
			ft_get(e);
		else
			write(e->cs, "ERROR: command unknown\n\0", 24);
		ft_free_tab(args);
	}
}

static void	ft_fork(t_env *e)
{
	pid_t	pid;
	pid_t	pid2;

	pid = fork();
	if (pid > 0)
		wait4(-1, NULL, 0, NULL);
	if (pid == 0)
	{
		pid2 = fork();
		if (pid2 > 0)
			exit(0);
		if (pid2 == 0)
		{
			get_cmd(e);
			printf("Client #%d quit\n", e->nc);
			close(e->cs);
			exit(0);
		}
	}
}

void		main_loop(t_env *e)
{
	struct sockaddr_in	sin;
	socklen_t			cslen;
	int					nc;

	nc = 1;
	cslen = sizeof(sin);
	while (42)
	{
		e->cs = E(-1, accept(e->s, (struct sockaddr *)&sin, &cslen), "accept");
		printf("Client #%d connected\n", nc);
		e->nc = nc++;
		ft_fork(e);
	}
}
