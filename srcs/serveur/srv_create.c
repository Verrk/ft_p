/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   srv_create.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/18 05:38:57 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/28 15:07:10 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

void		srv_create(t_env *e)
{
	struct protoent		*pe;
	struct sockaddr_in	sin;

	pe = (struct protoent *)EV(NULL, getprotobyname("tcp"), "getprotobyname");
	e->s = E(-1, socket(PF_INET, SOCK_STREAM, pe->p_proto), "socket serveur");
	sin.sin_family = AF_INET;
	sin.sin_port = htons(e->port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	E(-1, bind(e->s, (struct sockaddr *)&sin, sizeof(sin)), "bind");
	E(-1, listen(e->s, 42), "listen");
}
