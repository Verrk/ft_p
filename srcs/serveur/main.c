/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 22:28:50 by cpestour          #+#    #+#             */
/*   Updated: 2015/06/10 00:15:24 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

void			ft_exit(int sig)
{
	(void)sig;
	ft_putstr("Server quit\n");
	exit(0);
}

int				main(int ac, char **av)
{
	t_env		e;

	signal(SIGINT, ft_exit);
	get_opt(&e, ac, av);
	srv_create(&e);
	ft_putstr("Server open\n");
	main_loop(&e);
	return (0);
}
