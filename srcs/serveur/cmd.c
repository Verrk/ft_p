/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 14:46:57 by cpestour          #+#    #+#             */
/*   Updated: 2015/09/09 19:22:50 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

void		ft_get(t_env *e)
{
	char	**args;
	int		fd;
	int		r;
	char	buf[1024];

	args = ft_strsplit_space(e->buf);
	if (args[1])
	{
		if ((fd = open(args[1], O_RDONLY)) < 0)
			write(e->cs, "KO\0", 3);
		else
		{
			write(e->cs, "OK\0", 3);
			while ((r = read(fd, buf, 1023)) > 0)
			{
				buf[r] = 0;
				write(e->cs, buf, r);
			}
			close(fd);
			read(e->cs, buf, 3);
			write(e->cs, "SUCCESS: get\n\0", 14);
		}
	}
}

void		ft_put(t_env *e)
{
	char	**args;
	int		fd;
	int		r;
	char	buf[1024];
	char	*file;

	args = ft_strsplit_space(e->buf);
	if (!args[1])
		return ;
	file = ft_strrchr(args[1], '/');
	file = !file ? args[1] : file + 1;
	ft_read(e->cs, buf);
	if (ft_strcmp("OK", buf) == 0)
	{
		fd = open(file, FLAGS, MODE);
		while ((r = read(e->cs, buf, 1023)) > 0)
		{
			buf[r] = 0;
			ft_putstr_fd(buf, fd);
			if (r < 1023)
				break ;
		}
		close(fd);
		write(e->cs, "SUCCESS: put\n\0", 14);
	}
}

void		ft_ls(t_env *e)
{
	pid_t	pid;
	char	**args;
	int		fd[2];
	int		status;

	fd[0] = e->cs;
	fd[1] = e->cs;
	args = ft_strsplit_space(e->buf);
	if (args[1])
	{
		write(e->cs, "ERROR: ls: too many arg\n\0", 25);
		return ;
	}
	if ((pid = fork()) == 0)
	{
		dup2(fd[0], 1);
		dup2(fd[1], 2);
		execv("/bin/ls", args);
		write(e->cs, "ERROR: ls\n\0", 11);
		exit(-1);
	}
	else
		wait4(-1, &status, 0, NULL);
	if (status == 0)
		write(e->cs, "SUCCESS: ls\n\0", 13);
}

static int	ft_chdir(t_env *e, char *path)
{
	char	oldpath[MAXPATHLEN];
	char	newpath[MAXPATHLEN];

	getcwd(oldpath, MAXPATHLEN);
	if (chdir(path))
		return (0);
	getcwd(newpath, MAXPATHLEN);
	if (!ft_strprefix(e->rootdir, newpath))
	{
		chdir(oldpath);
		return (0);
	}
	e->currdir = newpath;
	return (1);
}

void		ft_cd(t_env *e)
{
	if (ft_strcmp(e->buf, "cd") == 0)
	{
		chdir(e->rootdir);
		e->currdir = e->rootdir;
		write(e->cs, "SUCCESS: cd\n\0", 13);
	}
	else if (ft_chdir(e, &(e->buf[3])) == 0)
		write(e->cs, "ERROR: cd\n\0", 11);
	else
		write(e->cs, "SUCCESS: cd\n\0", 13);
}
