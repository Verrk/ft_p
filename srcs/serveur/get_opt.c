/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_opt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/18 05:35:00 by cpestour          #+#    #+#             */
/*   Updated: 2015/06/09 23:52:51 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

void		get_opt(t_env *e, int ac, char **av)
{
	if (ac != 2)
	{
		fprintf(stderr, "Usage: %s <port>\n", av[0]);
		exit(1);
	}
	e->port = ft_atoi(av[1]);
	e->rootdir = getcwd(NULL, 0);
	e->currdir = e->rootdir;
}
