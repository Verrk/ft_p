/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   e.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/18 04:49:47 by cpestour          #+#    #+#             */
/*   Updated: 2015/12/18 18:10:20 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>
#include <errno.h>

void		*e_void(void *err, void *res, char *str)
{
	if (res == err)
	{
		fprintf(stderr, "%s error: %s\n",
				str, strerror(errno));
		exit(1);
	}
	return (res);
}

int			e_int(int err, int res, char *str)
{
	if (res == err)
	{
		fprintf(stderr, "%s error: %s\n",
				str, strerror(errno));
		exit(1);
	}
	return (res);
}

int			ft_read(int fd, char *buf)
{
	char	tmp[1024];
	int		i;
	int		c;

	ft_bzero(tmp, 1024);
	ft_bzero(buf, 1024);
	c = 0;
	while ((i = read(fd, tmp, 1) > 0) && tmp[0] != '\0')
	{
		ft_strcat(buf, tmp);
		c++;
		if (c == 1024)
			return (1024);
	}
	return (i);
}
